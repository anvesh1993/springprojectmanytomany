package com.example.manytomany2;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeveloperRepository<Developer> extends JpaRepository<Developer, Long>{
    
}